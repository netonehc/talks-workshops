# Talks Workshops

<div align="center">
<img src="/Imagenes/team-logo.png" height="150" width="150">
</div>

Aquí podreis encontrar las slides que hemos preparado para las charlas y los recursos especificos para cada una de ellas.

# Whoami

NetON EHC is a non-profit club of students, junior and senior open to
knowledge, dialogue and collaboration that aims to divulge, educate and raise
awareness about the fundamental role that computer security plays on
day-to-day of companies and citizens. Hosted at [UPV university](https://www.upv.es) (Valencia, Spain).

Find us at https://netonehc.org